﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorApp
{
    public partial class TimeEntry : Form
    {
        public TimeEntry(string patientid)
        {
            InitializeComponent();
            txtregid.Text = patientid;
            datepicker.MinDate = DateTime.Now.Date;
            datepicker.MaxDate = DateTime.Now.AddDays(30).Date;
            loadpatientinfo(patientid);
        }

        public void loadpatientinfo(string patientid)
        {
            DbHelper d = new DbHelper();
            string sqlQuery = "select * from tbl_PatientRegistration where reg_id='" + patientid + "'";
            DataSet ds = d.ExecuteDataSet(sqlQuery, ConnectionState.CloseOnExit);
            DataTable dtStore = ds.Tables[0];
            DataView dv = new DataView();
            dv = dtStore.DefaultView;
            if (dv.Count > 0)
            {
                lbpatientphone.Text = "Phone Number: "+dv[0]["patient_phone"].ToString();
                lbpstientname.Text = "Patient Name: "+dv[0]["patient_name"].ToString();
            }
        }
        private void btnsave_Click(object sender, EventArgs e)
        {
             bool status=validateAllFields();
            if (status == true)
            {
                AddTimeEntry();
            }
        }
        public void AddTimeEntry()
        {
            DbHelper d = new DbHelper();
            string sql_query = "INSERT INTO tbl_TimeEntry(price,reg_id,appintment_date,Time,hospital_name,Status,doc_id,admin_comment) " +
                " OUTPUT INSERTED.Time_id " +
                "values (@price,@reg_id,@appintment_date,@Time,@hospital_name,@Status,@doc_id,@admin_comment)";

            d.AddParameter("@price", txtprice.Text.ToString());
            d.AddParameter("@reg_id", txtregid.Text.ToString());
            d.AddParameter("@appintment_date", Convert.ToDateTime(datepicker.Value.ToString()).ToShortDateString());
            d.AddParameter("@Time", cmbtime.SelectedItem.ToString());
            d.AddParameter("@hospital_name", cmbhospitalname.SelectedItem.ToString());
            d.AddParameter("@Status", cmbstatus.SelectedItem.ToString());
            d.AddParameter("@doc_id", 2);
            d.AddParameter("@admin_comment", txtadmincomment.Text.Trim());

            Int32 time_id = (Int32)d.ExecuteScalar(sql_query, ConnectionState.CloseOnExit);
            AwaitingPatients t = new AwaitingPatients();
            t.MdiParent = this.MdiParent;
            t.WindowState = FormWindowState.Maximized;
            this.Close();
            t.Show();
        }
        public bool validateAllFields()
        {
            bool status = true;
            foreach (Control c1 in this.Controls)
            {
                if (c1 is ComboBox)
                {
                    ComboBox b1 = (c1) as ComboBox;
                    if (b1.SelectedIndex == -1)
                    {
                        MessageBox.Show(c1.Name + " Field cannnot be blank");
                        c1.BackColor = Color.Orange;
                        clear(c1 as ComboBox);
                        c1.Focus();
                        status = false;

                    }
                    else
                    {
                        // MessageBox.Show("Correct");
                        c1.BackColor = Color.White;
                        status = true;
                    }
                }
                else if(c1 is TextBox)
                {
                    if (string.IsNullOrEmpty(c1.Text)==true)
                    {
                        MessageBox.Show(c1.Name + " Field cannnot be blank");
                        c1.BackColor = Color.Orange;
                        clear2(c1 as TextBox);
                        c1.Focus();
                        status = false;

                    }
                    else
                    {
                        // MessageBox.Show("Correct");
                        c1.BackColor = Color.White;
                        status = true;
                    }
                }
            }
            return status;
        }
        private void clear2(TextBox t1)
        {
            t1.Text = "";
        }
        private void clear(ComboBox t1)
        {
            t1.SelectedIndex = 0;
        }
    }

}
