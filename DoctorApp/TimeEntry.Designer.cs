﻿namespace DoctorApp
{
    partial class TimeEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtadmincomment = new System.Windows.Forms.TextBox();
            this.cmbhospitalname = new System.Windows.Forms.ComboBox();
            this.cmbtime = new System.Windows.Forms.ComboBox();
            this.cmbstatus = new System.Windows.Forms.ComboBox();
            this.btnsave = new System.Windows.Forms.Button();
            this.datepicker = new System.Windows.Forms.DateTimePicker();
            this.txtregid = new System.Windows.Forms.TextBox();
            this.lbpstientname = new System.Windows.Forms.Label();
            this.lbpatientphone = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(101, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hospital Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(101, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Date";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(104, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Admin Comment";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(101, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Status";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(101, 158);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(141, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(202, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Add Patient Time Entry";
            // 
            // txtadmincomment
            // 
            this.txtadmincomment.Location = new System.Drawing.Point(200, 258);
            this.txtadmincomment.Multiline = true;
            this.txtadmincomment.Name = "txtadmincomment";
            this.txtadmincomment.Size = new System.Drawing.Size(276, 89);
            this.txtadmincomment.TabIndex = 7;
            // 
            // cmbhospitalname
            // 
            this.cmbhospitalname.FormattingEnabled = true;
            this.cmbhospitalname.Items.AddRange(new object[] {
            "National Hospital",
            "Jinnah Hospital",
            "Doctor Hospital"});
            this.cmbhospitalname.Location = new System.Drawing.Point(200, 99);
            this.cmbhospitalname.Name = "cmbhospitalname";
            this.cmbhospitalname.Size = new System.Drawing.Size(210, 21);
            this.cmbhospitalname.TabIndex = 8;
            this.cmbhospitalname.Text = "--Select Hospital Name--";
            // 
            // cmbtime
            // 
            this.cmbtime.FormattingEnabled = true;
            this.cmbtime.Items.AddRange(new object[] {
            "8:00 am to 9:00 am",
            "9:00 am to 10:00 am",
            "10:00 am to 11:00 am",
            "11:00 am to 12:00 pm",
            "12:00 pm to 01:00 pm",
            "01:00 pm to 02:00 pm",
            "02:00 pm to 03:00 pm",
            "03:00 pm to 04:00 pm",
            "04:00 pm to 05:00 pm",
            "05:00 pm to 06:00 pm",
            "06:00 pm to 07:00 pm",
            "07:00 pm to 08:00 pm",
            "08 :00 pm to 09:00 pm",
            "09:00 pm to 10:00 pm",
            "10:00 pm to 11:00 pm",
            "11:00 pm  to 12:00 am"});
            this.cmbtime.Location = new System.Drawing.Point(200, 158);
            this.cmbtime.Name = "cmbtime";
            this.cmbtime.Size = new System.Drawing.Size(210, 21);
            this.cmbtime.TabIndex = 10;
            this.cmbtime.Text = "--Select Time--";
            // 
            // cmbstatus
            // 
            this.cmbstatus.FormattingEnabled = true;
            this.cmbstatus.Items.AddRange(new object[] {
            "Awaiting",
            "Check in",
            "In Progress",
            "Check out"});
            this.cmbstatus.Location = new System.Drawing.Point(200, 186);
            this.cmbstatus.Name = "cmbstatus";
            this.cmbstatus.Size = new System.Drawing.Size(210, 21);
            this.cmbstatus.TabIndex = 11;
            this.cmbstatus.Text = "--Select Status--";
            // 
            // btnsave
            // 
            this.btnsave.Location = new System.Drawing.Point(200, 367);
            this.btnsave.Name = "btnsave";
            this.btnsave.Size = new System.Drawing.Size(75, 23);
            this.btnsave.TabIndex = 12;
            this.btnsave.Text = "Save";
            this.btnsave.UseVisualStyleBackColor = true;
            this.btnsave.Click += new System.EventHandler(this.btnsave_Click);
            // 
            // datepicker
            // 
            this.datepicker.Location = new System.Drawing.Point(200, 133);
            this.datepicker.Name = "datepicker";
            this.datepicker.Size = new System.Drawing.Size(210, 20);
            this.datepicker.TabIndex = 13;
            // 
            // txtregid
            // 
            this.txtregid.CausesValidation = false;
            this.txtregid.Location = new System.Drawing.Point(350, 16);
            this.txtregid.Name = "txtregid";
            this.txtregid.Size = new System.Drawing.Size(60, 20);
            this.txtregid.TabIndex = 14;
            this.txtregid.Visible = false;
            // 
            // lbpstientname
            // 
            this.lbpstientname.AutoSize = true;
            this.lbpstientname.Location = new System.Drawing.Point(104, 43);
            this.lbpstientname.Name = "lbpstientname";
            this.lbpstientname.Size = new System.Drawing.Size(35, 13);
            this.lbpstientname.TabIndex = 15;
            this.lbpstientname.Text = "label7";
            // 
            // lbpatientphone
            // 
            this.lbpatientphone.AutoSize = true;
            this.lbpatientphone.Location = new System.Drawing.Point(104, 74);
            this.lbpatientphone.Name = "lbpatientphone";
            this.lbpatientphone.Size = new System.Drawing.Size(35, 13);
            this.lbpatientphone.TabIndex = 16;
            this.lbpatientphone.Text = "label8";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(101, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Price";
            // 
            // txtprice
            // 
            this.txtprice.Location = new System.Drawing.Point(200, 220);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(210, 20);
            this.txtprice.TabIndex = 18;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(416, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "PKR";
            // 
            // TimeEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 419);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtprice);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lbpatientphone);
            this.Controls.Add(this.lbpstientname);
            this.Controls.Add(this.txtregid);
            this.Controls.Add(this.datepicker);
            this.Controls.Add(this.btnsave);
            this.Controls.Add(this.cmbstatus);
            this.Controls.Add(this.cmbtime);
            this.Controls.Add(this.cmbhospitalname);
            this.Controls.Add(this.txtadmincomment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "TimeEntry";
            this.Text = "TimeEntry";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtadmincomment;
        private System.Windows.Forms.ComboBox cmbhospitalname;
        private System.Windows.Forms.ComboBox cmbtime;
        private System.Windows.Forms.ComboBox cmbstatus;
        private System.Windows.Forms.Button btnsave;
        private System.Windows.Forms.DateTimePicker datepicker;
        private System.Windows.Forms.TextBox txtregid;
        private System.Windows.Forms.Label lbpstientname;
        private System.Windows.Forms.Label lbpatientphone;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.Label label8;
    }
}