﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorApp
{
    public partial class searchPatients : Form
    {
        public searchPatients()
        {
            InitializeComponent();
            gridview.RowHeaderMouseClick += new DataGridViewCellMouseEventHandler(gridview_RowHeaderMouseClick);
        }
        public void loadpatients()
        {
            DbHelper d = new DbHelper();
            string sqlQuery = "";
            if (string.IsNullOrEmpty(txtsearch.Text) == false && checkBox1.Checked == true)
            {
                sqlQuery = "select reg_id,patient_name,patient_phone,appintment_date,hospital_name,price,status from vw_TimeEntry where (patient_name like '%" + txtsearch.Text + "%' or patient_phone like '%" + txtsearch.Text + "%' or patient_IdCard like '%" + txtsearch.Text + "%') and appintment_date between '" + Convert.ToDateTime(dateTimePicker1.Value).ToShortDateString() + "' AND '" + Convert.ToDateTime(dateTimePicker2.Value).ToShortDateString() + "' order by Created_date";
            }
            else if (string.IsNullOrEmpty(txtsearch.Text) == true && checkBox1.Checked == true)
            {
                sqlQuery = "select reg_id,patient_name,patient_phone,appintment_date,hospital_name,price,status from vw_TimeEntry where appintment_date between '" + Convert.ToDateTime(dateTimePicker1.Value).ToShortDateString() + "' AND '" + Convert.ToDateTime(dateTimePicker2.Value).ToShortDateString() + "' order by Created_date";
          
            }
            else if (string.IsNullOrEmpty(txtsearch.Text) == false && checkBox1.Checked == false)
            {
                sqlQuery = "select reg_id,patient_name,patient_phone,appintment_date,hospital_name,price,status from vw_TimeEntry where (patient_name like '%" + txtsearch.Text + "%' or patient_phone like '%" + txtsearch.Text + "%' or patient_IdCard like '%" + txtsearch.Text + "%')  order by Created_date";
           
            }
            if (string.IsNullOrEmpty(sqlQuery) == false)
            {
                lbsearch.Text = "";
                DataSet ds = d.ExecuteDataSet(sqlQuery, ConnectionState.CloseOnExit);
                DataTable dtStore = ds.Tables[0];
                DataTable dtAll = ds.Tables[0].Copy();
                for (var i = 1; i < ds.Tables.Count; i++)
                {
                    dtAll.Merge(ds.Tables[i]);
                }
                gridview.AutoGenerateColumns = true;
                gridview.DataSource = dtAll;
            }
            else
            {
                lbsearch.Text = "Please add search parameters.";
            }
        }
        private void gridview_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            string s = gridview.Rows[e.RowIndex].Cells[0].Value.ToString();
            if (string.IsNullOrEmpty(s) == false)
            {
                TimeEntry t = new TimeEntry(s);
                //TimeEntry t = new TimeEntry("1");
                t.MdiParent = this.MdiParent;
                t.WindowState = FormWindowState.Maximized;
                this.Close();
                t.Show();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked==true)
            {
                panel1.Visible = true;
            }
            else
            {
                panel1.Visible = false;
            }
        }

        private void btnsearch_Click(object sender, EventArgs e)
        {
            loadpatients();
        }
    }
}
