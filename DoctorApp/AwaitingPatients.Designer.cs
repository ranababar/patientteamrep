﻿namespace DoctorApp
{
    partial class AwaitingPatients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label6 = new System.Windows.Forms.Label();
            this.gridview = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbpatientname = new System.Windows.Forms.Label();
            this.lbname = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbstatus = new System.Windows.Forms.ComboBox();
            this.btnupdate = new System.Windows.Forms.Button();
            this.lbid = new System.Windows.Forms.Label();
            this.cmbstatus2 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridview)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(275, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(151, 24);
            this.label6.TabIndex = 11;
            this.label6.Text = "Awaiting Patients";
            // 
            // gridview
            // 
            this.gridview.AllowUserToAddRows = false;
            this.gridview.AllowUserToDeleteRows = false;
            this.gridview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridview.Location = new System.Drawing.Point(75, 215);
            this.gridview.Name = "gridview";
            this.gridview.Size = new System.Drawing.Size(745, 150);
            this.gridview.TabIndex = 12;
            // 
            // panel1
            // 
            this.panel1.AccessibleDescription = "d";
            this.panel1.Controls.Add(this.lbid);
            this.panel1.Controls.Add(this.btnupdate);
            this.panel1.Controls.Add(this.cmbstatus);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lbname);
            this.panel1.Controls.Add(this.lbpatientname);
            this.panel1.Location = new System.Drawing.Point(189, 53);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(401, 135);
            this.panel1.TabIndex = 13;
            this.panel1.Visible = false;
            // 
            // lbpatientname
            // 
            this.lbpatientname.AutoSize = true;
            this.lbpatientname.Location = new System.Drawing.Point(52, 42);
            this.lbpatientname.Name = "lbpatientname";
            this.lbpatientname.Size = new System.Drawing.Size(71, 13);
            this.lbpatientname.TabIndex = 0;
            this.lbpatientname.Text = "Patient Name";
            // 
            // lbname
            // 
            this.lbname.AutoSize = true;
            this.lbname.Location = new System.Drawing.Point(143, 20);
            this.lbname.Name = "lbname";
            this.lbname.Size = new System.Drawing.Size(0, 13);
            this.lbname.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Status";
            // 
            // cmbstatus
            // 
            this.cmbstatus.FormattingEnabled = true;
            this.cmbstatus.Items.AddRange(new object[] {
            "Awaiting",
            "Check in",
            "In Progress",
            "Check out",
            "Cancel"});
            this.cmbstatus.Location = new System.Drawing.Point(116, 68);
            this.cmbstatus.Name = "cmbstatus";
            this.cmbstatus.Size = new System.Drawing.Size(121, 21);
            this.cmbstatus.TabIndex = 3;
            // 
            // btnupdate
            // 
            this.btnupdate.Location = new System.Drawing.Point(116, 106);
            this.btnupdate.Name = "btnupdate";
            this.btnupdate.Size = new System.Drawing.Size(75, 23);
            this.btnupdate.TabIndex = 4;
            this.btnupdate.Text = "Update Status";
            this.btnupdate.UseVisualStyleBackColor = true;
            this.btnupdate.Click += new System.EventHandler(this.btnupdate_Click);
            // 
            // lbid
            // 
            this.lbid.AutoSize = true;
            this.lbid.Location = new System.Drawing.Point(55, 19);
            this.lbid.Name = "lbid";
            this.lbid.Size = new System.Drawing.Size(0, 13);
            this.lbid.TabIndex = 5;
            // 
            // cmbstatus2
            // 
            this.cmbstatus2.FormattingEnabled = true;
            this.cmbstatus2.Items.AddRange(new object[] {
            "Awaiting",
            "Check in",
            "In Progress"});
            this.cmbstatus2.Location = new System.Drawing.Point(75, 188);
            this.cmbstatus2.Name = "cmbstatus2";
            this.cmbstatus2.Size = new System.Drawing.Size(141, 21);
            this.cmbstatus2.TabIndex = 14;
            this.cmbstatus2.Text = "--Please select Status--";
            this.cmbstatus2.SelectedIndexChanged += new System.EventHandler(this.cmbstatus2_SelectedIndexChanged);
            // 
            // AwaitingPatients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 377);
            this.Controls.Add(this.cmbstatus2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gridview);
            this.Controls.Add(this.label6);
            this.Name = "AwaitingPatients";
            this.Text = "AwaitingPatients";
            this.Load += new System.EventHandler(this.AwaitingPatients_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridview)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView gridview;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbstatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbname;
        private System.Windows.Forms.Label lbpatientname;
        private System.Windows.Forms.Button btnupdate;
        private System.Windows.Forms.Label lbid;
        private System.Windows.Forms.ComboBox cmbstatus2;

    }
}