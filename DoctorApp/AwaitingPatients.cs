﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorApp
{
    public partial class AwaitingPatients : Form
    {
        public AwaitingPatients()
        {
            InitializeComponent();
            loadpatients();
            cmbstatus2.SelectedIndex = 0;
            gridview.RowHeaderMouseClick += new DataGridViewCellMouseEventHandler(gridview_RowHeaderMouseClick);
        }

        public void loadpatients()
        {
             DbHelper d = new DbHelper();
             string sqlQuery = "select reg_id,patient_name,patient_phone,appintment_date,hospital_name,price,status,Time_id from vw_TimeEntry where status='" + cmbstatus2.SelectedItem + "' and appintment_date='" + Convert.ToDateTime(DateTime.Now).ToShortDateString() + "' order by Created_date";
            DataSet ds = d.ExecuteDataSet(sqlQuery, ConnectionState.CloseOnExit);
            DataTable dtStore = ds.Tables[0];
            DataTable dtAll = ds.Tables[0].Copy();
            for (var i = 1; i < ds.Tables.Count; i++)
            {
                dtAll.Merge(ds.Tables[i]);
            }
            gridview.AutoGenerateColumns = true;
            gridview.DataSource = dtAll;
            
        }
        private void AwaitingPatients_Load(object sender, EventArgs e)
        {
            
        }
        //dataGridView1 RowHeaderMouseClick Event  
        private void gridview_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            panel1.Visible = true;
           // lbid.Visible = false;
            lbid.Text = gridview.Rows[e.RowIndex].Cells[7].Value.ToString();            
            lbpatientname.Text = "Patient Name: " + gridview.Rows[e.RowIndex].Cells[1].Value.ToString();
            cmbstatus.SelectedItem = gridview.Rows[e.RowIndex].Cells[6].Value.ToString();
        }

        private void btnupdate_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(lbid.Text) == false)
            {
                updatepatientstatus();
                loadpatients();
                panel1.Visible = false;
            }
        }
        public void updatepatientstatus()
        {
            DateTime Time = DateTime.Now;

            string sql_query = "";


            if (cmbstatus.SelectedItem == "Check in")
            {
                sql_query = "Update tbl_TimeEntry Set status ='" + cmbstatus.SelectedItem + "',checkin_Time='" + Time.ToString("h tt") + "' " +
               " Where (Time_id = " + lbid.Text + ")";
            }
            else if (cmbstatus.SelectedItem == "In Progress")
            {
                sql_query = "Update tbl_TimeEntry Set status ='" + cmbstatus.SelectedItem + "',inprogress_time='" + Time.ToString("h tt") + "' " +
               " Where (Time_id = " + lbid.Text + ")";
            }
            else if (cmbstatus.SelectedItem == "Check out")
            {
                sql_query = "Update tbl_TimeEntry Set status ='" + cmbstatus.SelectedItem + "',checkout_time='" + Time.ToString("h tt") + "' " +
               " Where (Time_id = " + lbid.Text + ")";
            }
            else 
            {
                sql_query = "Update tbl_TimeEntry Set status ='" + cmbstatus.SelectedItem + "' " +
               " Where (Time_id = " + lbid.Text + ")";
            }
            DbHelper d = new DbHelper();          
            d.ExecuteNonQuery(sql_query, ConnectionState.CloseOnExit);
        }

        private void cmbstatus2_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadpatients();
        }
    }
}
