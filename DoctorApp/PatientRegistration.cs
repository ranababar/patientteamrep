﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorApp
{
    public partial class PatientRegistration : Form
    {
        public PatientRegistration()
        {
            InitializeComponent();
            //DbHelper d = new DbHelper();
            //string sqlQuery = "select * from tbl_LoginInfo";
            //DataSet ds = d.ExecuteDataSet(sqlQuery, ConnectionState.CloseOnExit);
            //DataTable dtStore = ds.Tables[0];
           // return server = dtStore.Rows[0]["Value"].ToString();

        }

        private void btnnext_Click(object sender, EventArgs e)
        {

          bool status=validateAllFields();
          if (status == true)
          {
              DbHelper d = new DbHelper();
              string sql_query = "INSERT INTO tbl_PatientRegistration(patient_name,patient_address,patient_phone,patient_IDCard) " +
                  " OUTPUT INSERTED.reg_id " +
                  "values (@patient_name,@patient_address,@patient_phone,@patient_IDCard)";

              d.AddParameter("@patient_name", txtname.Text.Trim());
              d.AddParameter("@patient_address", txtaddress.Text.Trim());
              d.AddParameter("@patient_phone", txtphone.Text.Trim());
              d.AddParameter("@patient_IDCard",txtidcard.Text.Trim());
            
              Int32 patient_regid = (Int32)d.ExecuteScalar(sql_query, ConnectionState.CloseOnExit);

              TimeEntry t = new TimeEntry(patient_regid.ToString());            
              //TimeEntry t = new TimeEntry("1");
              t.MdiParent = this.MdiParent;
              t.WindowState = FormWindowState.Maximized;
              this.Close();
              t.Show();
          }
        }
        public bool validateAllFields()
        {
            bool status = true;
            foreach (Control c1 in this.Controls)
            {
                if (c1 is TextBox)
                {
                    if (c1.Text == "")
                    {
                        MessageBox.Show(c1.Name + " Field cannnot be blank");
                        c1.BackColor = Color.Orange;
                        clear(c1 as TextBox);
                        c1.Focus();
                        status = false;

                    }
                    else
                    {
                       // MessageBox.Show("Correct");
                        c1.BackColor = Color.White;
                        status = true;
                    }
                }
            }
            return status;
        }
        private void clear(TextBox t1)
        {
            t1.Text = "";
        }
    
        }
        
    }

