﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DoctorApp
{
    public partial class patientInProgress : Form
    {
        public patientInProgress()
        {
            InitializeComponent();
            loadpatientinfo();
        }
        public void loadpatientinfo()
        {

            DbHelper d = new DbHelper();
            string sqlQuery = "select reg_id,patient_IdCard,Doctor_comment,patient_name,patient_phone,appintment_date,hospital_name,price,status from vw_TimeEntry where  status='In Progress' order by appintment_date";
            DataSet ds = d.ExecuteDataSet(sqlQuery, ConnectionState.CloseOnExit);
            DataTable dtStore = ds.Tables[0];
            DataView dv = new DataView();
            dv = dtStore.DefaultView;
            if (dv.Count > 0)
            {
                lbregid.Visible = false;
                lbregid.Text = dv[0]["reg_id"].ToString();
                lbpinfo.Text = "";
                lbpinfo.Text += "Patient Name: " + dv[0]["patient_name"].ToString() + Environment.NewLine + Environment.NewLine;
                lbpinfo.Text += "Contact No: " + dv[0]["patient_phone"].ToString() + Environment.NewLine + Environment.NewLine;
                lbpinfo.Text += "CNIC: " + dv[0]["patient_IdCard"].ToString() + Environment.NewLine + Environment.NewLine;
                for (int i = 0; i < dv.Count; i++)
                {
                   
                    if (dv[i]["status"].ToString() == "Check out" && Convert.ToDateTime(dv[i]["appintment_date"].ToString()).ToString("DD-MM-YYYY") != DateTime.Now.ToString("DD-MM-YYYY"))
                    {
                        lbpinfo.Text += "At: " + dv[i]["appintment_date"].ToString() + Environment.NewLine + Environment.NewLine;
                        lbpinfo.Text += "Comment: " + dv[i]["Doctor_comment"].ToString() + Environment.NewLine + Environment.NewLine;
                    }
                }
                //this.lbpinfo.Size = new System.Drawing.Size(398, 10);
                //this.lbpinfo.AutoSize = true;
            }
        }
        
    }

}
